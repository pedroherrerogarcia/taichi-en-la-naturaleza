---
date: 2023-12-28
description: "Sobre la comunidad de la vida"
featured_image: '2023-12-28HojasDeBosque--taichi-taijiquanPEQ.jpg'
tags: ["taijiquan","comunidad"]
title: "Hojas de Bosque"
---

{{< figure src="~/static/2023-12-28HojasDeBosque--taichi-taijiquanPEQ.jpg" title="CC BY-SA 3.0 Malene Thyssen" >}}

### La pertenencia a la comunidad de la vida comparando la unión de las hojas de los árboles a la rama que las sostiene, les da la vida y les da el sentido.

Somos parte de la comunidad de la vida. Como las hojas de los árboles están unidas al árbol, y el árbol es parte del bosque, nosotros dependemos de la tierra, de la comunidad de los vivientes. Pasamos, como las hojas caen con la estación en su momento, pero el árbol permanece. Semejanzas, paralelismos para recuperar una actitud de reconocimiento, un sentimiento de pertenencia a nuestro entorno, que está vivo como lo estamos nosotros. Debería dolernos el sufrimiento de la tierra, igual que nos duele cuando nos ponemos enfermos. Y hemos de actuar en consecuencia, cuidando activamente el ecosistema vivo, defendiéndolo y previniendo los posibles daños que puedan sobrevenirle.

### El arte del taijiquan, y el qigong se comparan a un gran árbol que une a la comunidad de practicantes en todo el mundo.

Las hojas del árbol intercambian energía con el entorno y lo hacen crecer. Cada practicante crece con su arte y alimenta a la comunidad que se beneficia cuando lo enseña y lo comparte. Las distintas especies de árboles como diferentes comunidades, todas extienden sus ramas y hojas hacia la luz del Sol. Todas hunden sus raíces en el conocimiento transmitido de generación en generación desde la antigüedad hasta nuestros días. Se sostienen gracias a este conocimiento, pero no es algo estático, es un arte vivo que se adapta al momento presente.

### El taichi en la naturaleza es una llamada a habitar el aquí y ahora, ayudando, aportando los beneficios de este arte en este momento en que como sociedad necesitamos adaptarnos en la crisis global.

Entrenar en una sala agradable está bien, pero salir al campo, al bosque, y practicar el arte del taichi y el chikung al aire libre es algo realmente especial. Gracias a nuestro arte podemos conectar, o más bien reconectar, con la naturaleza. Aprender a sentirnos de nuevo en el bosque como en casa es como regresar. Volver de un viaje extraño, desde una lejana y desconocida tierra para poder, al fin respirar tranquilos, bajar la guardia y descansar. Sentir que no estamos solos, que todo está en su sitio y está bien. Que somos uno más entre muchos, que nos "hablan" y nos "escuchan" continuamente, aunque hemos pasado mucho tiempo eligiendo no escuchar sus voces y reconocer sus gestos. Que somos parte de la comunidad de la vida, y siempre está comunicándose con nosotros si queremos cambiar de actitud y abrir nuestros sentidos para recibir el mensaje.

El taijiquan nos ayuda a cultivar un bienestar general, holístico. No sólo nos sentimos en forma físicamente, y mentalmente despiertos, también poco a poco nuestras emociones se hacen más estables y centradas. El miedo a vivir se hace pequeño, y la asertividad y la alegría crecen. El mundo tiende a devolver, como reflejado, aquello que proyectamos en él, y así a nuestro alrededor las cosas positivas se hacen más presentes, y los elementos negativos tienden a evitarnos.

Como aquellos míticos "guerreros del arcoiris", los y las artistas marciales del taijiquan, y otras artes semejantes, están en mi opinión ante la oportunidad de ayudarnos a luchar por la conciencia. Para hablar sin miedo, decir las cosas con claridad y a la vez mantener la tranquila firmeza, la constancia y la ecuanimidad que pueden contribuir a que como sociedad tomemos mejores decisiones.

El taichi en la naturaleza es una llamada a sentirnos vivamente como una hoja en el arbol. Adaptándose al viento, absorbiendo la energía y canalizándola. Firmemente sujetos, y a la vez con la máxima libertad de movimiento.
