---
title: "Baños de bosque"
date: 2023-12-29
description: "Taijiquan en el bosque"
cascade:
  featured_image: '2023-12-29BosqueGredos--taichi-taijiquan.jpg'
tags: ["taijiquan","baños de bosque"]
---
{{< figure src="home/content/static/2023-12-29BosqueGredos--taichi-taijiquan.jpg" title="Bosque en Gredos, Ávila" >}}

### Los baños de bosque son actividades para el cultivo del bienestar


Los baños de bosque, o [shinrin-yoku  (森林浴) ](https://es.wikipedia.org/wiki/Ba%C3%B1o_de_bosque) son recorridos por el bosque manteniendo una actitud abierta, con los sentidos despiertos, para (re)conectar con la naturaleza. En realidad vale salir al campo, aunque no estemos totalmente rodeados de árboles. El entorno natural que tengas cerca puede ser el lugar en el que abrirse a la interacción y la comunicación con el resto de los seres vivos que te rodean y con los que formáis la "comunidad de los vivientes". Ir al bosque con esta disposición con frecuencia es toda una actividad de cultivo del bienestar, con claros [efectos beneficiosos para la salud](https://habladelbosque.es/banos-bosque-shinrin-yoku/)
{{< figure src="../../../static/2023-12-29BosqueCaminando--taichi-taijiquan.jpg" title="bosque con una persona caminando" >}}

### El taichi y el chikung (qigong), son actividades para el cultivo del bienestar, que combinan muy bien con los baños de bosque

El qigong es un arte de desarrollo personal, en el que se armonizan los diversos aspectos, ya sean físicos, emocionales o mentales en un todo armonioso. Pero no sólo nos ayuda a elevar el tono vital en general, aumentando nuestra energía. También nos permite conectarnos con el entorno. El Maestro Jordi Vila i Oliveras dice en su libro "Curso de qigong. Teoría y Práctica" vol. II, p. 238 que "Combinando distintos métodos ... se pueden sentir las conexiones existentes entre nosotros y el Universo circundante." Y es que la consciencia de sí mismo se puede extender al entorno cercano. La sensación subjetiva de conexión con lo que me rodea cuando practicamos en el espacio verde de El Soto los sábados por la mañana es de hecho muy real.

Jordi continua mencionando los baños de bosque (森林浴), comparando a los árboles con la energía de la naturaleza, y el efecto beneficioso que obtienen quienes los practican. La imagen de un árbol, la idea de enraizamiento, como un modelo energético son comunes en el taijiquan y el qigong. Algunos de los ejercicios de qigong que explica son ejercicios a realizar con árboles, identificándose con ellos y unificando la intención para "despertar, mediante la intercesión de la energía del árbol, un sentimiento puro de conexión (...) que nos unifique con la energía del árbol y por ende, del Cielo y de la Tierra (...) un sentimiento amoroso y sutil de unidad, bienestar, armonía y quietud. (...) un sentimiento ecologista desde el corazón."

En nuestras clases abiertas de los sábados practicamos tanto qigong como taijiquan, y tenemos muy presente el entorno natural de árboles centenarios que nos rodean. Al volver la atención hacia lo interno, curiosamente se acentúan las percepciones del entorno. El sol sobre la piel, el aire alrededor, la luz verde al trasluz de las hojas y los sonidos de los pájaros o las personas. La vida vibrando, como siempre lo ha hecho, y la repentina consciencia de su presencia potente. Y está ahí siempre, para cuando queramos.

