---
date: 2023-12-28
description: "Un punto de encuentro para el taijiquan"
featured_image: "2023-12-29ElSoto--taichi-taijiquanPEQ.png"
tags: ["taijiquan","clases"]
title: "Clase abierta en El Soto"
---

{{< figure src="~/static/2023-12-29ElSoto--taichi-taijiquanPEQ.png" title="CC BY-SA 4.0 Pedro Herrero" >}}

Si quieres saber lo que es el taichi, o más propiamente escrito taijiquan, acude los sábados al pulmón verde de Ávila. Quedamos en El Soto, cerca del aparcamiento.

No importa que no tengas conocimientos de taichi, puedes participar cualquiera que sea tu nivel.

No es una clase remunerada, funciona como un punto de encuentro en el que compartir nuestros conocimientos y prácticas. Por lo tanto puedes venir tanto para aprender como para enseñar. Nos interesan no sólo el taichi, sino otras artes relacionadas como por ejemplo el chikung o qigong, el fengshui, el xinjiquan, o el baguaquan...

Deja tus datos de contacto en el formulario de la web, y te diremos exactamente cuándo quedaremos la próxima vez. 

¡Anímate y participa!