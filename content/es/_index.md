---
title: "Hojas de Bosque"
description: "Taijiquan y Qigong en la Naturaleza"
cascade:
  featured_image: 'Pedro_taijiquaneando.jpeg'
---

{{< figure src="./Pedro_taijiquaneando.jpeg" title="Taijiquan en la Naturaleza" >}}

Ven a practicar taijiquan y qigong al aire libre. Siéntete parte de la comunidad de seres vivos que te rodea, siéntete en contacto con la Naturaleza.

Advertencia: este sitio está en construcción, por favor sea paciente
