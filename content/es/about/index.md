---
title: "Quién soy yo"
subtitle: "Para que sepas algo del que escribe"
description: "Información sobre mí, y las cosas que hago"
comments: false
featured_image: 'Pedro_taijiquaneando.jpeg'
menu:
  main:
    weight: 1
---

{{< figure src="../Pedro_taijiquaneando.jpeg" title="Taijiquan en la Naturaleza" >}}

Soy Pedro Herrero García.

- Estudié biología en Oviedo, con un año en Irlanda como Erasmus
- Vivo en Ávila, y soy profe de taijiquan
- Me gusta andar y leer
- Soy fan de GNU/linux, el código libre y el conocimiento libre
- Participo y apoyo al movimiento de las ciudades en transición, y te animo a que te actives como ciudadan@ y ayudes

### Actualmente

Estoy centrado en mi trabajo de formación en taijiquan, y también como guía en la naturaleza y educador ambiental.
Mi familia y mis amigos ocupan el resto de mi tiempo.