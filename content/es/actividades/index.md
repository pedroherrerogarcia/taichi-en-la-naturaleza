---
title: "Actividades"
subtitle: "Cómo llevamos a cabo las actividades"
description: "Actividades, talleres y clases"
comments: false
featured_image: '20221002PedroActuando.jpg'
menu:
  main:
    weight: 1
---

{{< figure src="~/static/20221002PedroActuando.jpg" title="" >}}

### Clases de Taijiquan en la Naturaleza

{{< figure src="static/2023 Cartel Clases de Tai Chi en la Naturaleza.png" title="" >}}