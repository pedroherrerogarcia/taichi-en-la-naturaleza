---
title: "Who I am"
subtitle: "So that you know something of the writer"
description: "Information about me, and the things I do"
comments: false
featured_image: 'Pedro_taijiquaneando.jpeg'
menu:
  main:
    weight: 1
---
{{< figure src="~/content/es/Pedro_taijiquaneando.jpeg" title="Taijiquan amidst Nature" >}}

I am Pedro Herrero García.

- I studied biology in Oviedo, with a year in Ireland as an Erasmus student.
- I live in Avila, and I am a taijiquan teacher.
- I like walking and reading
- I'm a fan of GNU/linux, free code and free knowledge
- I participate and support the Transition Towns movement, and I encourage you to become active as a citizen and to help

### Currently

I am focused on my work as a taijiquan trainer, nature guide and environmental educator. My family and friends occupy the rest of my time.