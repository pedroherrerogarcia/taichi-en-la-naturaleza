---
title: "Forest Bathing"
description: "Taijiquan amidst Forest"
date: 2024-04-22
cascade:
  featured_image: '2023-12-29BosqueGredos--taichi-taijiquan.jpg'
tags: ["taijiquan", "forest bathing"]
---

{{< figure src="home/content/static/2023-12-29BosqueGredos--taichi-taijiquan.jpg" title="Bosque en Gredos, Ávila" >}}

### Forest baths are activities for the cultivation of well-being.

Forest baths, or [shinrin-yoku (森林浴) ](https://en.wikipedia.org/wiki/Shinrin-yoku) are walks through the forest with an open attitude, with the senses awake, to (re)connect with nature. In reality, it is worth going out into the countryside, even if we are not totally surrounded by trees. The natural environment close to you can be the place where you can open yourself to interaction and communication with the other living beings around you and with those who form the "community of the living". Going to the forest with this disposition on a regular basis is an activity of cultivating well-being, with clear [beneficial health effects](https://habladelbosque.es/banos-bosque-shinrin-yoku/)
{{< figure src="home/static/2023-12-29BosqueCaminando--taichi-taijiquan.jpg" title="forest with a person walking" >}}

### Taichi and chikung (qigong) are activities for the cultivation of well-being, which combine very well with forest bathing.

Qigong is an art of personal development, in which the various aspects, whether physical, emotional or mental, are harmonised into a harmonious whole. But it not only helps us to raise our overall vital tone, increasing our energy. It also allows us to connect with the environment. Master Jordi Vila i Oliveras says in his book "Curso de qigong. Theory and Practice" vol. II, p. 238 that "By combining different methods ... we can feel the connections between us and the surrounding Universe". And it is that self-awareness can be extended to the immediate environment. The subjective feeling of connection with my surroundings when we practice in the green space of El Soto on Saturday mornings is indeed very real.

Jordi goes on to mention forest baths (森林浴), comparing trees to the energy of nature, and the beneficial effect they have on those who practice them. The image of a tree, the idea of grounding, as an energy model is common in taijiquan and qigong. Some of the qigong exercises he explains are exercises to be performed with trees, identifying with them and unifying the intention to "awaken, through the intercession of the tree's energy, a pure feeling of connection (...) that unifies us with the energy of the tree and thus of Heaven and Earth (...) a loving and subtle feeling of unity, well-being, harmony and stillness (...) an ecological feeling from the heart."

In our Saturday open classes we practice both qigong and taijiquan, and we are very mindful of the natural environment of ancient trees that surrounds us. As we turn our attention inward, the perceptions of the environment are curiously heightened. The sun on our skin, the air around us, the green light shining through the leaves and the sounds of birds or people. Life vibrating, as it always has, and the sudden awareness of its potent presence. And it is always there, whenever we want it to be.