---
date: 2023-12-28
description: "A meeting point for taijiquan"
featured_image: ""
tags: ["taijiquan","lessons"]
title: "Open class in El Soto"
---

If you want to know what taichi is, or more properly written taijiquan, come to the green lung of Ávila on Saturdays. Meet at El Soto, near the car park.

It doesn't matter if you have no knowledge of taichi, you can participate regardless of your level.

It is not a paid class, it works as a meeting point to share our knowledge and practices. So you can come to learn as well as to teach. We are interested not only in taichi, but also in other related arts such as chikung or qigong, fengshui, xinjiquan, baguaquan...

Leave your contact details in the form on the website, and we will tell you exactly when we will meet next time.

Come and join us!