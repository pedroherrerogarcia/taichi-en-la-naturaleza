---
date: 2023-12-28
description: "On the community of Life"
featured_image: "2023-12-28HojasDeBosque--taichi-taijiquanPEQ.jpg"
tags: ["taijiquan","community"]
title: "Forest leaves"
---

{{< figure src="home/content/static/2023-12-28HojasDeBosque--taichi-taijiquanPEQ.jpg" title="CC BY-SA 3.0 Malene Thyssen" >}}

### Belonging to the community of life, comparing the union of the leaves of the trees to the branch that supports them, gives them life and gives them meaning.

We are part of the community of life. As the leaves of the trees are attached to the tree, and the tree is part of the forest, we are dependent on the earth, on the community of the living. We pass, as the leaves fall with the season in their time, but the tree remains. Similarities, parallels to recover an attitude of recognition, a feeling of belonging to our environment, which is alive as we are. We should be pained by the suffering of the earth, just as we are pained when we fall ill. And we must act accordingly, actively caring for the living ecosystem, defending it and preventing possible damage to it.

### The art of taijiquan, and qigong are likened to a great tree that unites the community of practitioners all over the world.

The leaves of the tree exchange energy with the environment and make it grow. Each practitioner grows with his or her art and nourishes the community that benefits when he or she teaches and shares it. Different species of trees like different communities, all extend their branches and leaves towards the sunlight. They are all rooted in knowledge passed down from generation to generation from ancient times to the present day. They are sustained by this knowledge, but it is not static, it is a living art that adapts to the present moment.

### Taichi in nature is a call to inhabit the here and now, helping, bringing the benefits of this art at a time when we as a society need to adapt in the global crisis.

Training in a nice room is fine, but going out into the countryside, into the forest, and practising the art of taichi and chikung in the open air is something really special. Thanks to our art we can connect, or rather reconnect, with nature. Learning to feel at home in the forest again is like coming back. Coming back from a strange journey, from a distant and unknown land, to finally be able to breathe easy, let our guard down and rest. To feel that we are not alone, that everything is in its place and all is well. That we are one among many, that they "speak" to us and "listen" to us continuously, even though we have spent a long time choosing not to listen to their voices and recognise their gestures. That we are part of the community of life, and it is always communicating with us if we want to change our attitude and open our senses to receive the message.

Taijiquan helps us to cultivate an overall, holistic well-being. Not only do we feel physically fit and mentally awake, but also our emotions gradually become more stable and focused. Fear of living becomes small, and assertiveness and joy grow. The world tends to return, as if reflected, what we project onto it, and so the positive things around us become more present, and the negative elements tend to avoid us.

Like those mythical "rainbow warriors", the martial artists of taijiquan and other such arts, I believe, have the opportunity to help us fight for consciousness. To speak without fear, to say things clearly, while maintaining the calm firmness, steadfastness and equanimity that can help us as a society to make better decisions.

Taichi in nature is a call to feel vividly like a leaf on a tree. Adapting to the wind, absorbing the energy and channelling it. Firmly held, yet with maximum freedom of movement.