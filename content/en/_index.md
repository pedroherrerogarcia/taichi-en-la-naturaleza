---
title: "Hojas de Bosque"

description: "Taijiquan y Qigong amidst Nature"
cascade:
  featured_image: 'Pedro_taijiquaneando.jpeg'
---

{{< figure src="~/content/es/Pedro_taijiquaneando.jpeg" title="Taijiquan amidst Nature" >}}

Come and practice taijiquan and qigong in the open air. Feel part of the community of living beings that surrounds you, feel in contact with nature.

Warning: this site is under construction, please be patient