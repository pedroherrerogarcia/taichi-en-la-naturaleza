---
title: "Feuilles de Bois"

description: "Taijiquan et Qigong dans la Nature"
cascade:
  featured_image: 'Pedro_taijiquaneando.jpeg'
---

{{< figure src="~/content/es/Pedro_taijiquaneando.jpeg" title="Taijiquan dans la Nature" >}}

Venez pratiquer le taijiquan et le qigong en plein air. Sentez-vous appartenir à la communauté d'êtres vivants qui vous entoure, sentez-vous en contact avec la nature.

Attention : ce site est en construction, merci de patienter.