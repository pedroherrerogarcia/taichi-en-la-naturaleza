---
date: 2023-12-28
description: "Un point de recontre pour le taijiquan"
featured_image: '2023-12-29ElSoto--taichi-taijiquanPEQ.png'
tags: ["taijiquan","clases"]
title: "Clase ouverte à El Soto"
---
{{< figure src="~/static/2023-12-29ElSoto--taichi-taijiquanPEQ.png" title="Classe ouverte à El Soto" >}}

Si vous voulez savoir ce qu'est le taichi, ou plus exactement le taijiquan, venez dans le poumon vert d'Ávila le samedi. Rendez-vous à El Soto, près du parking.

Peu importe que vous n'ayez aucune connaissance du taichi, vous pouvez participer quel que soit votre niveau.

Il ne s'agit pas d'un cours payant, mais d'un point de rencontre pour partager nos connaissances et nos pratiques. Vous pouvez donc venir pour apprendre et pour enseigner. Nous nous intéressons non seulement au taichi, mais aussi à d'autres arts connexes tels que le chikung ou qigong, le fengshui, le xinjiquan, le baguaquan...

Laissez vos coordonnées dans le formulaire sur le site web, et nous vous dirons exactement quand nous nous rencontrerons la prochaine fois.

Venez nous rejoindre !