---
title: "Bains de Forêt"
date: 2024-04-22
description: "Taijiquan dans la Forêt"
cascade:
  featured_image: '2023-12-29BosqueGredos--taichi-taijiquan.jpg'
tags: ["taijiquan", "bains de forêt"]
---

{{< figure src="~/static/2023-12-29BosqueGredos--taichi-taijiquan.jpg" title="Bois à Gredos, Ávila" >}}

### Les bains de forêt sont des activités visant à cultiver le bien-être.

Les bains de forêt, ou [shinrin-yoku (森林浴) ](https://fr.wikipedia.org/wiki/Sylvoth%C3%A9rapie) sont des promenades en forêt l'esprit ouvert, les sens en éveil, pour se (re)connecter à la nature. En réalité, il vaut la peine de sortir dans la nature, même si nous ne sommes pas totalement entourés d'arbres. L'environnement naturel proche de vous peut être le lieu où vous pouvez vous ouvrir à l'interaction et à la communication avec les autres êtres vivants qui vous entourent et avec ceux qui forment la "communauté des vivants". Se rendre régulièrement en forêt dans cet état d'esprit est une activité de culture du bien-être, dont les [bénéfices pour la santé] sont évidents (https://habladelbosque.es/banos-bosque-shinrin-yoku/).

{{< figure src="~/static/2023-12-29BosqueCaminando--taichi-taijiquan.jpg" title="forêt avec une personne qui marche" >}}

### Le taichi et le chikung (qigong) sont des activités visant à cultiver le bien-être, qui se combinent très bien avec les bains de forêt.

Le qigong est un art de développement personnel, dans lequel les différents aspects, qu'ils soient physiques, émotionnels ou mentaux, sont harmonisés en un tout harmonieux. Mais il ne nous aide pas seulement à élever notre tonus vital global, en augmentant notre énergie. Il nous permet également de nous connecter à l'environnement. Le maître Jordi Vila i Oliveras dit dans son livre "Curso de qigong. Theory and Practice" vol. II, p. 238 que "En combinant différentes méthodes ... nous pouvons ressentir les liens qui nous unissent à l'Univers qui nous entoure". Et c'est ainsi que la conscience de soi peut être étendue à l'environnement immédiat. Le sentiment subjectif de connexion avec mon environnement lorsque nous pratiquons dans l'espace vert d'El Soto le samedi matin est en effet très réel.

Jordi poursuit en mentionnant les bains de forêt (森林浴), comparant les arbres à l'énergie de la nature et à l'effet bénéfique qu'ils ont sur ceux qui les pratiquent. L'image d'un arbre, l'idée de l'enracinement, en tant que modèle énergétique est courante dans le taijiquan et le qigong. Certains des exercices de qigong qu'il explique sont des exercices à réaliser avec des arbres, en s'identifiant à eux et en unifiant l'intention de "réveiller, par l'intercession de l'énergie de l'arbre, un pur sentiment de connexion (...) qui nous unifie avec l'énergie de l'arbre et donc du Ciel et de la Terre (...) un sentiment aimant et subtil d'unité, de bien-être, d'harmonie et d'immobilité (...) un sentiment écologique venant du cœur".

Lors de nos cours ouverts du samedi, nous pratiquons à la fois le qigong et le taijiquan, et nous sommes très attentifs à l'environnement naturel d'arbres anciens qui nous entoure. Lorsque nous tournons notre attention vers l'intérieur, les perceptions de l'environnement s'intensifient curieusement. Le soleil sur notre peau, l'air autour de nous, la lumière verte qui traverse les feuilles et le bruit des oiseaux ou des gens. La vie qui vibre, comme elle l'a toujours fait, et la conscience soudaine de sa puissante présence. Et elle est toujours là, quand nous le voulons.
