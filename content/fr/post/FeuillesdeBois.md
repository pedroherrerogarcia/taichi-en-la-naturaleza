---
date: 2023-12-28
description: "Sur la communauté de la vie"
featured_image: '2023-12-28HojasDeBosque--taichi-taijiquanPEQ.jpg'
tags: ["taijiquan","communauté"]
title: "Feuilles de bois"
---

{{< figure src="~/content/static/2023-12-28HojasDeBosque--taichi-taijiquanPEQ.jpg" title="Feuilles dans le bois" >}}

### L'appartenance à la communauté de vie, comparable à l'union des feuilles des arbres à la branche qui les soutient, leur donne vie et leur donne un sens.

Nous faisons partie de la communauté de la vie. Comme les feuilles des arbres sont attachées à l'arbre, et que l'arbre fait partie de la forêt, nous dépendons de la terre, de la communauté des vivants. Nous passons, comme les feuilles tombent avec la saison en leur temps, mais l'arbre reste. Des similitudes, des parallèles pour retrouver une attitude de reconnaissance, un sentiment d'appartenance à notre environnement, qui est vivant comme nous. Nous devrions être peinés par la souffrance de la terre, tout comme nous sommes peinés lorsque nous tombons malades. Et nous devons agir en conséquence, en prenant activement soin de l'écosystème vivant, en le défendant et en évitant qu'il ne soit endommagé.

### L'art du taijiquan et du qigong est comparé à un grand arbre qui unit la communauté des pratiquants du monde entier.

Les feuilles de l'arbre échangent de l'énergie avec l'environnement et le font grandir. Chaque praticien grandit avec son art et nourrit la communauté qui bénéficie de son enseignement et de son partage. Les différentes espèces d'arbres, comme les différentes communautés, étendent toutes leurs branches et leurs feuilles vers la lumière du soleil. Elles sont toutes enracinées dans un savoir transmis de génération en génération, depuis les temps anciens jusqu'à aujourd'hui. Elles sont soutenues par ce savoir, mais celui-ci n'est pas statique, c'est un art vivant qui s'adapte au moment présent.

### Le Taichi dans la nature est un appel à habiter l'ici et le maintenant, en aidant, en apportant les bénéfices de cet art à un moment où nous, en tant que société, avons besoin de nous adapter à la crise mondiale.

S'entraîner dans une belle salle, c'est bien, mais sortir dans la campagne, dans la forêt, et pratiquer l'art du taichi et du chikung en plein air, c'est quelque chose de vraiment spécial. Grâce à notre art, nous pouvons nous connecter, ou plutôt nous reconnecter, avec la nature. Réapprendre à se sentir chez soi dans la forêt, c'est comme revenir. Revenir d'un voyage étrange, d'une terre lointaine et inconnue, pour enfin pouvoir respirer à l'aise, baisser la garde et se reposer. Sentir que nous ne sommes pas seuls, que tout est à sa place et que tout va bien. Que nous sommes un parmi tant d'autres, qu'ils nous "parlent" et nous "écoutent" en permanence, même si nous avons longtemps choisi de ne pas écouter leurs voix et de ne pas reconnaître leurs gestes. Que nous faisons partie de la communauté de la vie et qu'elle communique toujours avec nous si nous voulons changer notre attitude et ouvrir nos sens pour recevoir le message.

Le Taijiquan nous aide à cultiver un bien-être global et holistique. Non seulement nous nous sentons en bonne forme physique et mentalement éveillés, mais nos émotions deviennent progressivement plus stables et plus concentrées. La peur de vivre s'estompe, l'affirmation de soi et la joie augmentent. Le monde a tendance à nous renvoyer, comme un reflet, ce que nous projetons sur lui. Ainsi, les éléments positifs qui nous entourent deviennent plus présents et les éléments négatifs ont tendance à nous éviter.

Comme ces mythiques "guerriers arc-en-ciel", les artistes martiaux du taijiquan et d'autres arts de ce type ont, je crois, la possibilité de nous aider à lutter pour la conscience. Parler sans crainte, dire les choses clairement et en même temps maintenir le calme, la fermeté, la constance et l'équanimité qui peuvent nous aider, en tant que société, à prendre de meilleures décisions.

Le taichi dans la nature est un appel à se sentir vivement comme une feuille sur un arbre. S'adapter au vent, absorber l'énergie et la canaliser. En se tenant fermement, mais avec une liberté de mouvement maximale.
