---
title: "Qui suis-je ?"
subtitle: "Pour que vous sachiez quelque chose de l'écrivain"
description: "Informations sur moi et sur mes activités"
comments: false
featured_image: 'Pedro_taijiquaneando.jpeg'
menu:
  main:
    weight: 1
---

{{< figure src="~/content/es/Pedro_taijiquaneando.jpeg" title="Taijiquan dans la Nature" >}}

Je suis Pedro Herrero García.
- J'ai étudié la biologie à Oviedo et j'ai passé une année en Irlande en tant qu'étudiant Erasmus.
- Je vis à Avila et je suis professeur de taijiquan.
- J'aime marcher et lire
- Je suis un fan de GNU/linux, de code libre et de connaissance libre.
- Je participe et je soutiens le mouvement des villes en transition, et je vous encourage à devenir actif en tant que citoyen et à aider.

### Actuellement

Je me concentre sur mon travail de formateur en taijiquan, de guide de la nature et d'éducateur à l'environnement. Ma famille et mes amis occupent le reste de mon temps.
